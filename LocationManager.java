import android.content.Context;
import android.content.res.Resources;

import com.artemkopan.baseproject.internal.UiManager.RxLifeCycle;
import com.artemkopan.baseproject.rx.BaseRx;
import com.artemkopan.baseproject.utils.CollectionUtils;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

import static com.jabrool.data.location.query.PointsQuery.latLngFormat;


public class LocationManager extends BaseRx {

    @Inject @ApplicationContext Context mContext;
    @Inject RestService mRestService;
    @Inject Resources mRes;

    @Inject
    LocationManager(@RxOnDestroy Observable<RxLifeCycle> rxLifecycle) {
        super(rxLifecycle);
    }

    public Observable<RouteItem> getRoute(LatLng startPoint, LatLng endPoint,
                                          List<LatLng> wayPoints) {
        return getRoute(startPoint, endPoint, CollectionUtils.isEmpty(wayPoints)
                                              ? null
                                              : wayPoints.toArray(new LatLng[wayPoints.size()]));
    }

    /**
     * @return Return route points {@link List<LatLng>} with WayPoints order {@link List<Integer>}
     */
    public Observable<RouteItem> getRoute(LatLng startPoint, LatLng endPoint,
                                          LatLng... wayPoints) {
        return mRestService
                .getPolyline(
                        latLngFormat(startPoint),
                        latLngFormat(endPoint),
                        PointsQuery.createPoints(true, wayPoints),
                        mRes.getString(R.string.google_web_api_key))
                .compose(applyLifecycle())
                .compose(applySchedulers());
    }

    public Observable<AutocompleteAnswer> getPlacesAutoComplete(String input) {
        return mRestService.getPlacesAutoComplete(input, mRes.getString(R.string.google_web_api_key))
                .compose(applyLifecycle()).compose(applySchedulers());
    }

    public Observable<PlaceDetail> getPlaceDetails(String placeId) {
        return mRestService.getPlaceDetails(placeId, mRes.getString(R.string.google_web_api_key))
                .compose(applyLifecycle()).compose(applySchedulers());
    }

    public Observable<PlaceDetails> getPlaceDetails(double lat, double lng) {
        String latLng = lat + ", " + lng;
        return mRestService.getPlaceDetailsFromLocation(latLng, mRes.getString(R.string.google_web_api_key))
                .compose(applyLifecycle()).compose(applySchedulers());
    }

    public Observable<MatrixItem> getDestinationMatrix(LatLng originPoint, List<LatLng> destinationPoints) {
        return getDestinationMatrix(originPoint,
                destinationPoints.toArray(new LatLng[destinationPoints.size()]));
    }

    public Observable<MatrixItem> getDestinationMatrix(LatLng originPoint, LatLng... destinationPoints) {
        return mRestService
                .getDistanceMatrix(
                        PointsQuery.createPoints(false, originPoint).toString(),
                        PointsQuery.createPoints(false, destinationPoints).toString(),
                        mRes.getString(R.string.google_web_api_key))
                .compose(applyLifecycle())
                .compose(applySchedulers());
    }

    public Observable<MatrixItem> getDestinationMatrix(String[] originsPlaces, String[] destinationPlaces) {
        return mRestService
                .getDistanceMatrix(
                        PlacesQuery.create(originsPlaces).toString(),
                        PlacesQuery.create(destinationPlaces).toString(),
                        mRes.getString(R.string.google_web_api_key))
                .compose(applyLifecycle())
                .compose(applySchedulers());

    }

    /**
     * Work correctly only for matrix with one row!!!
     *
     * @param minRate in km. If distance < min rate, then distance = min rate;
     * @return Long value in km
     */
    public Observable<Float> calculateMatrixDistance(MatrixItem matrixItem, float minRate) {
        return Observable.create(e -> {
            List<RowsItem> rows = matrixItem.getRows();
            float distance = 0;
            if (rows != null && rows.size() > 0) {
                RowsItem rowItem = rows.get(0);
                if (rowItem.getElements() != null) {
                    for (ElementsItem elementsItem : rowItem.getElements()) {
                        if (elementsItem.getStatus().equals("ZERO_RESULTS")) {
                            e.onError(new Exception(mRes.getString(R.string.error_wrong_distance)));
                            return;
                        }
                        distance += elementsItem.getDistanceValue();
                    }
                }
            }
            distance = distance > 0 ? distance / 1000 : 0;
            if (distance <= minRate) {
                distance = minRate;
            }
            e.onNext(distance);
            e.onComplete();
        });
    }

}
