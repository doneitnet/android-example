import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.*

import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

class NewsFragment : BaseInjectFragment(), NewsView {

    companion object {

        private val INTERPOLATOR = FastOutSlowInInterpolator()

        fun route(): Router.FragmentTransactionBuilder {
            return Router.FragmentTransactionBuilder(NewsFragment())
        }

    }

    @Inject internal lateinit var presenter: NewsPresenter
    @Inject internal lateinit var rxPermissions: RxPermissions
    @Inject internal lateinit var rxEventBus: RxEventBus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        presentationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ActionBarHelper.builder()
                .setTitle(getString(R.string.news_title))
                .setNavigationIcon(R.drawable.ic_menu_white_24dp)
                .setNavigationClickListener({ MainActivity.openDrawer(activity) })
                .build()
                .configureActionBar(activity as AppCompatActivity, toolbar)

        swipeRefreshLayout.isEnabled = false

        RxViewClick.create(fab)
                .subscribe({
                    rxPermissions.requestCreateChallengePermissions()
                            .subscribe({
                                if (it) {
                                    CreateChallengeActivity.route()
                                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                            .startFrom(activity)
                                }
                            }, Throwable::printStackTrace)
                            .addTo(getDestroyDisposable())

                }, Throwable::printStackTrace)
                .addTo(getDestroyDisposable())

        newsList.apply {
            layoutManager = LinearLayoutManager(this@NewsFragment.activity)
            addItemDecoration(SpaceItemDecoration(dimen(R.dimen.activity_horizontal_half_margin), false, true, true))
            addOnScrollListener(FabRecyclerListener(fab))
        }

        rxEventBus.filteredObservable(ConnectionStateChanged::class.java)
                .subscribe { showConnectionEstablished(it.isConnected) }

        presenter.setupList(newsList)
        presenter.attachToView(this)
        presenter.getNewsFeed()
    }

    override fun showMessage(messageRes: Int, tag: Any?) {
        if (tag == MessageTags.TAG_CONNECTION_ERROR) {
            showConnectionEstablished(false)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_search -> {
                SearchActivity.route().startFrom(activity)
                true
            }
            else -> false
        }
    }

    override fun showUserProfile(id: Int, name: String?, avatar: String?) {
        ProfileActivity.route(id, name, avatar).startFrom(activity)
    }

    override fun showProgress(tag: Any?, message: String?) {
        swipeRefreshLayout?.isRefreshing = true
    }

    override fun hideProgress(tag: Any?) {
        swipeRefreshLayout?.isRefreshing = false
    }

    override fun showEmpty() {
        newsList.showText(R.string.news_feed_empty)
    }

    override fun showList() {
        newsList.hideAll()
    }

    override fun getPresenter(): Presenter<*>? {
        return presenter
    }

    private fun showConnectionEstablished(isConnected: Boolean) {
        newsMessage?.let {
                it.post {
                if (isConnected) {
                    if (swipeRefreshLayout.translationY > 0) {
                        ViewCompat.animate(swipeRefreshLayout)
                                .translationY(0F)
                                .setDuration(300)
                                .setInterpolator(INTERPOLATOR)
                                .withEndAction {
                                    newsMessage.visibility = View.INVISIBLE

                                    if (newsList.childCount == 0) {
                                        presenter.getNewsFeed()
                                    }
                                }
                    }
                } else {
                    if (swipeRefreshLayout.translationY == 0F) {
                        ViewCompat.animate(swipeRefreshLayout)
                                .translationY(newsMessage.height.toFloat() - swipeRefreshLayout.paddingTop)
                                .setDuration(300)
                                .setInterpolator(INTERPOLATOR)
                                .withEndAction {
                                    newsMessage.setText(R.string.news_feed_no_internet_connection)
                                    newsMessage.setTextColor(ContextCompat.getColor(activity, R.color.colorRed))
                                    newsMessage.visibility = View.VISIBLE
                                }
                    }
                }
            }
        }
    }
}