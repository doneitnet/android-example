import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.graphics.drawable.AnimatedVectorDrawableCompat
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.view.View
import android.view.ViewAnimationUtils

import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.widget_create_challenge.view.*
import timber.log.Timber
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class CreateChallengeView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : ConstraintLayout(context, attrs, defStyleAttr) {

    companion object {

        const val BLUR_RADIUS = 10
        const val SAMPLING = 1
        const val OFFSET_DELAY = 50L
        const val ANIM_VIEW_COUNT = 5

        private val INTERPOLATOR = FastOutSlowInInterpolator()
    }

    var onChallengeListener: OnChallengeListener? = null

    private val animations = Collections.newSetFromMap(ConcurrentHashMap<Animator, Boolean>())
    private val buttons: Array<View>
    private var isShow: Boolean = false

//    isPrivate var isKeyboardShow: Boolean = false
//    isPrivate val keyboardListener = KeyboardChecker()

    init {
        inflate(context, R.layout.widget_create_challenge, this)
        isTransitionGroup = true

        backgroundImg.callOnPreDraw {
            setDefaultValue(it)
            return@callOnPreDraw false
        }

        infoTxt.callOnPreDraw {
            setDefaultValue(it)
            return@callOnPreDraw false
        }

        buttons = arrayOf(challengesBtn, trophyBtn, careBtn)
        buttons.forEach {
            it.callOnPreDraw {
                it.setTag(R.id.pos_point, floatArrayOf(it.x, it.y))
                setDefaultValue(it)
                it.scaleX = 0f
                it.scaleY = 0f
                return@callOnPreDraw false
            }
        }

        challengesBtn.setOnClickListener {
            if (context != null) {
                CreateChallengeActivity.route().startFrom(context)
            }
        }

        fab.setOnClickListener {
            toggle()
        }

        backgroundImg.setOnClickListener {
            hide()
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (isShow) {
            drawBackground()
        }
    }

    private fun setDefaultValue(it: View) {
        it.visibility = GONE
        it.alpha = 0f
    }

    fun setTrophySubText(subText: CharSequence) {
        val builder = SpannableStringBuilder(context.getString(R.string.challenges_trophy))
        builder.setSpan(CustomTypefaceSpan("", FontUtils.selectTypeface(context, FontUtils.MEDIUM)),
                0, builder.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        builder.append('\n')
        builder.append(subText)

        trophyBtn.text = builder
    }

    fun toggle() {
        // exclude anchor view from blur background
        fab?.visibility = View.INVISIBLE

        if (isShow) {
            hide()
        } else {
            show()
        }

        fab?.visibility = View.VISIBLE
    }

    fun show() {
        if (isShow) return
//      if (isKeyboardShow)

        ExtraUtils.hideKeyboard(this)
        cancelAnim()

        isShow = true
        onChallengeListener?.onChangeState(isShow)

        if (backgroundImg != null) {
            drawBackground()
        }

        anim()
    }

    fun hide() {
        if (!isShow) return
        cancelAnim()

        isShow = false
        onChallengeListener?.onChangeState(isShow)

        anim()
    }

    private fun drawBackground() {
        Blurry.with(context)
                .radius(BLUR_RADIUS)
                .sampling(SAMPLING)
                .animate(AnimUtils.FAST_DURATION)
                .capture(parent as View)
                .into(backgroundImg)
    }

    private fun cancelAnim() {
        val it = animations.iterator()
        while (it.hasNext()) {
            it.next().cancel()
            it.remove()
        }
    }

    private fun anim() {

        val value: Float
        val buttons: Array<View>

        if (isShow) {
            value = 1f
            buttons = this.buttons
        } else {
            value = 0f
            buttons = this.buttons.reversedArray()
        }

        playBackgroundAnim(value)
        playInfoAnim(value)
        playFabAnimation()

        buttons.forEachIndexed { index, view ->
            val scaleX = ObjectAnimator.ofFloat(view, View.SCALE_X, value)
            val scaleY = ObjectAnimator.ofFloat(view, View.SCALE_Y, value)
            val alpha = ObjectAnimator.ofFloat(view, View.ALPHA, value)

            val pos = view.getTag(R.id.pos_point) as FloatArray

            Timber.i("${Arrays.toString(pos)} :Fab left  ${fab.left} Fab Right ${fab.right}")

            val deltaX = Math.abs(pos[0] - fab.left)
            val currentX = view.translationX
            val deltaY = Math.abs(pos[1] - fab.top)
            val currentY = view.translationY

            val transitionX: ObjectAnimator = if (isShow) {
                ObjectAnimator.ofFloat(view, View.TRANSLATION_X, if (currentX == 0f) deltaX else currentX, 0f)
            } else {
                ObjectAnimator.ofFloat(view, View.TRANSLATION_X, if (currentX != 0f) currentX else 0f, deltaX)
            }
            val transitionY: ObjectAnimator = if (isShow) {
                ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, if (currentY == 0f) deltaY else currentY, 0f)
            } else {
                ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, if (currentY != 0f) currentY else 0f, deltaY)
            }

            val animator = AnimatorSet()
            animator.playTogether(scaleX, scaleY, transitionX, transitionY, alpha)
            animator.addListener(AnimListener(view))
            animator.startDelay = (index * OFFSET_DELAY)
            animator.setTarget(view)
            animator.interpolator = INTERPOLATOR
            animator.start()
        }
    }


    private fun playInfoAnim(value: Float) {
        val animator = AnimatorSet()
        val alpha = ObjectAnimator.ofFloat(infoTxt, View.ALPHA, value)

        if (isShow) {
            val scaleY = ObjectAnimator.ofFloat(infoTxt, View.SCALE_Y, 1f, 1.1f, 1f)
            val scaleX = ObjectAnimator.ofFloat(infoTxt, View.SCALE_X, 1f, 1.1f, 1f)
            scaleY.startDelay = OFFSET_DELAY
            scaleX.startDelay = OFFSET_DELAY
            animator.playTogether(scaleX, scaleY, alpha)
            animator.startDelay = 4 * OFFSET_DELAY
        } else {
            animator.playTogether(alpha)
        }

        animator.setTarget(infoTxt)
        animator.addListener(AnimListener(infoTxt))
        animator.start()
    }

    private fun playBackgroundAnim(value: Float) {
        val animator = AnimatorSet()
        val alpha = ObjectAnimator.ofFloat(infoTxt, View.ALPHA, value)

        alpha.duration = AnimUtils.FAST_DURATION.toLong()

        // get the center for the clipping circle
        val cx = (fab.left + fab.right) / 2
        val cy = (fab.top + fab.bottom) / 2

        val fabRadius = Math.max(fab.width, fab.height).toFloat()

        // get the final radius for the clipping circle
        val bgRadius = Math.hypot(Math.max(cx, backgroundImg.width - cx).toDouble(),
                Math.max(cy, backgroundImg.height - cy).toDouble()).toFloat()

        val reveal = if (isShow) {
            ViewAnimationUtils.createCircularReveal(backgroundImg, cx, cy, fabRadius, bgRadius)
        } else {
            ViewAnimationUtils.createCircularReveal(backgroundImg, cx, cy, bgRadius, fabRadius)
        }

        reveal.duration = AnimUtils.MIDDLE_DURATION.toLong()

        animator.playTogether(alpha, reveal)
        animator.setTarget(backgroundImg)
        animator.addListener(object : AnimListener(backgroundImg) {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                if (!isShow) view.background = null
            }

        })

        if (!isShow) {
            animator.startDelay = ANIM_VIEW_COUNT * OFFSET_DELAY
        }

        animator.start()
    }

    private fun playFabAnimation() {
        val vec = AnimatedVectorDrawableCompat.create(context,
                if (isShow) R.drawable.avd_plus_to_cross else R.drawable.avd_cross_to_plus)
        fab.setImageDrawable(vec)
        vec?.start()
    }


    interface OnChallengeListener {

        fun onChangeState(isShow: Boolean)

    }

    private open inner class AnimListener(val view: View) : AnimatorListenerAdapter() {

        override fun onAnimationEnd(animation: Animator) {
            if (!isShow) view.visibility = View.GONE
            animations.remove(animation)
        }

        override fun onAnimationStart(animation: Animator) {
            if (isShow) view.visibility = View.VISIBLE
            animations.add(animation)
        }
    }

//    isPrivate inner class KeyboardChecker : ViewTreeObserver.OnGlobalLayoutListener {
//        override fun onGlobalLayout() {
//            if (!isShow || !viewTreeObserver.isAlive) return
//
//            // navigation bar height
//            var navigationBarHeight = 0
//            val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
//            if (resourceId > 0) {
//                navigationBarHeight = resources.getDimensionPixelSize(resourceId)
//            }
//
//            // status bar height
//            val statusBarHeight = ExtraUtils.getStatusBarHeight(context)
//
//            // display window size for the app layout
//            val rect = Rect()
//            ViewUtils.getActivity(this@CreateChallengeView)?.window?.decorView?.getWindowVisibleDisplayFrame(rect)
//
//            // screen height - (user app height + status + nav) ..... if non-zero, then there is a soft keyboard
//            val keyboardHeight = this@CreateChallengeView.rootView.height - (statusBarHeight + navigationBarHeight + rect.height())
//
//            Timber.d("Keyboard show $keyboardHeight")
//            val isKeyboardShow = keyboardHeight > 0
//
//            if (this@CreateChallengeView.isKeyboardShow == isKeyboardShow) return
//
//            //update main variable
//            this@CreateChallengeView.isKeyboardShow = isKeyboardShow
//
//            if (isKeyboardShow) ExtraUtils.hideKeyboard(this@CreateChallengeView)
//
//            // redraw blur background
//            if (!isKeyboardShow && isShow) {
//                drawBackground()
//            }
//        }
//
//    }

}