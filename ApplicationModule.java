import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.location.Address;
import android.provider.Settings.Secure;
import android.text.TextUtils;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import timber.log.Timber;

/**
 * Provide application-level dependencies. Mainly singleton object that can be injected from
 * anywhere in the app.
 */
@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    @ApplicationContext
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    Resources provideRes(@ApplicationContext Context context) {
        return context.getResources();
    }

    @SuppressLint("HardwareIds")
    @Provides
    @Singleton
    @DeviceId
    String provideDeviceUid(@ApplicationContext Context context) {
        String id = FirebaseInstanceId.getInstance().getId();
        Timber.i("provideDeviceUid(): " + id);
        if (TextUtils.isEmpty(id)) {
            id = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        }
        return id;
    }

    @Provides
    @Singleton
    SettingsManager provideSettings(@ApplicationContext Context context, DeliveryStartController startController) {
        return new SettingsManager(context, startController);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return BaseService.Factory.buildGson();
    }

    @Provides
    @Singleton
    ConnectivityInterceptor provideConnectivityInterceptor(@ApplicationContext Context context) {
        return new ConnectivityInterceptor(context);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(ConnectivityInterceptor interceptor) {
        return BaseService.Factory.buildOkHttp(interceptor);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient client) {
        return BaseService.Factory.createClient(gson, client);
    }

    @Provides
    @Singleton
    RestService provideRestService(Retrofit retrofit) {
        return retrofit.create(RestService.class);
    }

    @Provides
    @Singleton
    WebSocketManager provideWebSocketManager(Gson gson, @DeviceId String deviceUid) {
        return new WebSocketManager(gson, deviceUid);
    }

    @Provides
    @Singleton
    @Named(ALL)
    ObservableList<RequestItem> provideAllRequests() { return new ObservableList<>(); }

    @Provides
    @Singleton
    @Named(ACCEPTED)
    ObservableList<RequestItem> provideAcceptedRequests() { return new ObservableList<>(); }

    @Provides
    @Singleton
    @FavoriteLocations
    ObservableList<Address> provideFavorite() { return new ObservableList<>(); }

    @Provides
    @Singleton
    DeliveryStartController provideDeliveryTimer() { return new DeliveryStartController(); }

    @Provides
    @Singleton
    RxEventBus provideRxEvenBus() {return new RxEventBus();}

    @Provides
    @Singleton
    @NotificationsGlobal
    AtomicInteger provideNotificationGlobalCount() {return new AtomicInteger(0);}

    @Provides
    @Singleton
    @NotificationsMessage
    AtomicInteger provideNotificationMessageCount() {return new AtomicInteger(0);}

}
