import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface ChatService {

    @GET("posts/{id}/chats")
    fun getChatDetails(@Path("id") id: Int): Single<ChatRoomResponse>

    @POST("posts/{id}/chat_messages")
    fun sendMessage(@Path("id") id: Int, @Body request: MessageRequest): Completable

    @PUT("posts/{id}/mark_all_as_read")
    fun markAllAsRead()

    @DELETE("posts/{id}/chats")
    fun deleteChat()

}