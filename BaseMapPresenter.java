import android.Manifest.permission;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.view.MotionEvent;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

import static com.jabrool.base.Constants.Map.ZOOM_DEFAULT;


@SuppressWarnings("WeakerAccess")
public abstract class BaseMapPresenter<T extends BaseMapView> extends BasePresenterImpl<T> {

    @Nullable protected GoogleMap mMap;
    protected EventLocationUpdate mCurrentLocation;
    protected boolean mFocus;
    protected boolean mFirstLocation;

    protected final Context mContext;

    private Map<Marker, Pair<ObjectAnimator, ObjectAnimator>> mMarkerAnimations;
    private boolean mAvailablePlayServices;

    protected BaseMapPresenter(Context context, RxEventBus eventBus, Observable<RxLifeCycle> onDestroySubject) {
        mContext = context;
        eventBus.filteredObservable(EventLocationUpdate.class)
                .takeUntil(onDestroySubject)
                .subscribe(this::onEventLocationUpdate);
        initGoogleMaps();
    }

    @Override
    public void detachView() {
        if (mMap != null) {
            mMap.clear();
            mMap = null;
        }
        super.detachView();
    }

    //==============================================================================================
    // Map functions
    //==============================================================================================
    public void initGoogleMaps() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(mContext);

        if (resultCode == ConnectionResult.SUCCESS) {
            mAvailablePlayServices = true;
            MapsInitializer.initialize(mContext);
        }
    }

    public void setMapTouchListener(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                mFocus = false;
                break;
        }
    }

    /**
     * Set google map, when it was ready
     */
    public void setMap(@NonNull GoogleMap map) {
        mFirstLocation = true;
        mMap = map;
        mMap.getUiSettings().setTiltGesturesEnabled(false);
    }

    /**
     * Move and zoom to my location
     */
    public void moveToMyLocation() {
        mFocus = true;
        if (mCurrentLocation != null) {
            moveToLocation(mCurrentLocation.getLatLng(), true, true);
        }
    }

    /**
     * Move camera to current location
     *
     * @param position      current position
     * @param zoomToDefault if true - then was used value {@link com.jabrool.base.Constants} ZOOM_DEFAULT
     * @param animate       if true - then move and zoom was animated;
     */
    protected void moveToLocation(LatLng position, boolean zoomToDefault, boolean animate) {
        if (mMap == null) return;
        CameraPosition oldCamPos = mMap.getCameraPosition();
        CameraUpdate cameraUpdateFactory;
        if (oldCamPos != null) {
            CameraPosition newCamPos = new CameraPosition(position,
                                                          zoomToDefault ? ZOOM_DEFAULT : oldCamPos.zoom,
                                                          oldCamPos.tilt,
                                                          oldCamPos.bearing);

            cameraUpdateFactory = CameraUpdateFactory.newCameraPosition(newCamPos);
        } else {
            cameraUpdateFactory = CameraUpdateFactory.newLatLngZoom(position, ZOOM_DEFAULT);
        }

        if (animate) {
            mMap.animateCamera(cameraUpdateFactory, Constants.Map.ANIM_DURATION, null);
        } else {
            mMap.moveCamera(cameraUpdateFactory);
        }
    }

    /**
     * Zoom map to all points;
     */
    protected void zoomToBounds(LatLng... latLngs) {
        CommonUtils.zoomToPoints(mMap, mContext.getResources().getDimensionPixelSize(R.dimen.map_bounds_padding),
                                 true, latLngs);
    }

    //==============================================================================================
    // Event update listener
    //==============================================================================================

    private void onEventLocationUpdate(EventLocationUpdate event) {
        mCurrentLocation = event;
        if (mAvailablePlayServices) {
            onLocationUpdate(event);
        }
    }

    protected abstract void onLocationUpdate(EventLocationUpdate event);

    //==============================================================================================
    // Other functional
    //==============================================================================================

    /**
     * Create marker with car icon R.drawable.ic_map_marker;
     */
    protected MarkerOptions createCarMarker(LatLng latLng) {
        return new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(
                        R.drawable.ic_map_marker))
                .anchor(.5f, .5f)
                .flat(true)
                .zIndex(99)
                .position(latLng);
    }

    protected MarkerOptions createDefaulMarker(@DrawableRes int markerRes, LatLng pos) {
        return new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(markerRes))
                .position(pos);
    }

    /**
     * Calculate angle from two points;
     */
    protected float calculateAngle(LatLng fromPos, LatLng toPos) {
        float heading = 0;

        if (fromPos != null) {
            heading = (float) SphericalUtil.computeHeading(fromPos, toPos);
            heading = (heading + 360) % 360;
        }

        return heading;
    }

    //==============================================================================================
    // Animation
    //==============================================================================================

    /**
     * @return Observable for check permission {@link permission#ACCESS_COARSE_LOCATION}, {@link permission#ACCESS_FINE_LOCATION}
     */
    protected Observable<Boolean> getPermissionObservable() {
        return RxPermissions.getInstance(mContext)
                            .request(permission.ACCESS_COARSE_LOCATION, permission.ACCESS_FINE_LOCATION)
                            .filter(aBoolean -> aBoolean);
    }

    protected void animateMarker(@Nullable Marker marker, LatLng location, float angle) {

        if (marker == null ||
            marker.getPosition() == null ||
            (marker.getPosition().equals(location) && marker.getRotation() == angle)) {
            return;
        }

        if (mMarkerAnimations == null) mMarkerAnimations = new HashMap<>();

        Pair<ObjectAnimator, ObjectAnimator> markerAnimation = mMarkerAnimations.get(marker);

        ObjectAnimator markerPosAnim = null;
        ObjectAnimator markerRotationAnim = null;

        if (markerAnimation != null) {
            markerPosAnim = markerAnimation.first;
            markerRotationAnim = markerAnimation.second;
        }

        if (markerPosAnim == null || markerRotationAnim == null) {

            markerPosAnim = MarkerAnimator.positionAnimator(marker, location);
            markerPosAnim.setDuration(Constants.Map.ANIM_DURATION);

            markerRotationAnim = MarkerAnimator.rotationAnimator(marker, angle);
            markerRotationAnim.setDuration(150);

            mMarkerAnimations.put(marker, Pair.create(markerPosAnim, markerRotationAnim));
        }

        markerPosAnim.setObjectValues(location);
        markerRotationAnim.setFloatValues(angle);

        markerPosAnim.start();
        markerRotationAnim.start();
    }

    @Nullable
    protected Marker addMarker(@DrawableRes int markerRes, LatLng position) {
        if (mMap == null || position == null) return null;
        return mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(markerRes))
                                                 .position(position));
    }

    protected Marker addMarkerAnim(@DrawableRes int markerRes, LatLng position) {
        return addMarkerAnim(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(markerRes))
                                                .position(position));
    }

    @SuppressWarnings("ConstantConditions")
    protected Marker addMarkerAnim(MarkerOptions markerOptions) {
        markerOptions.alpha(0);
        Marker marker = mMap.addMarker(markerOptions);
        ObjectAnimator animator = MarkerAnimator.alphaAnimator(marker, 0, 1);
        animator.setDuration(AnimUtils.FAST_DURATION).start();
        return marker;
    }

    protected void removeMarkerAnim(Marker marker) {
        ObjectAnimator animator = MarkerAnimator.alphaAnimator(marker, 1, 0);
        animator.setDuration(AnimUtils.FAST_DURATION)
                .addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animation.removeAllListeners();
                        marker.remove();
                    }
                });
        animator.start();
    }
}

