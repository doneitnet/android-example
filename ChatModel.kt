import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class ChatModel @Inject constructor(service: ChatService) : BaseModel() {

    private val chatService = service

    fun getChatDetails(id: Int): Single<ChatRoomResponse> {
        return chatService.getChatDetails(id)
                .checkApiErrorSingle()
                .applySchedulers()
    }

    fun sendMessage(id: Int, text: String): Completable {
        return chatService.sendMessage(id, MessageRequest(text))
                .checkApiErrorCompletable()
                .applySchedulers()
    }

}